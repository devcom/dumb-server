FROM python:3.6-alpine

WORKDIR /usr/src/app

COPY requirements.txt server.py ./
RUN mkdir -p /data && pip install --no-cache-dir -r requirements.txt
COPY sample/* /data/
RUN sed -i 's/\.\/sample\/server.pem/\/data\/server.pem/;s/\.\/sample//' /data/config.yml

VOLUME /data

CMD [ "python", "./server.py", "/data/config.yml" ]
