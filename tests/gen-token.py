#!/usr/bin/env python
# Description: Generates JWTs with the given claims using an HMAC secret.
#              The JWTs generated may be used with unit tests.
# Dependencies:
#   - pyJWT

import jwt
import os
import sys
import time

from os.path import basename

if len(sys.argv) < 2:
  print 'USAGE: {} sub'.format(basename(sys.argv[0]))
  sys.exit(0)

claims = {}
now = int(time.time())
claims['iat'] = now
claims['sub'] = sys.argv[1]
token = jwt.encode(claims, 'secret', algorithm='HS256')
print token
