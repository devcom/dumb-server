import io
import server
import unittest
from unittest import mock
from email.message import Message


class TestModuleFunctions(unittest.TestCase):
    """Test cases for server module functions."""

    def test_parse_params(self):
        params = server.parse_params("a=1&b=2&c=3")
        self.assertEqual(3, len(params))
        self.assertEqual('1', params['a'])
        self.assertEqual('2', params['b'])
        self.assertEqual('3', params['c'])

    def test_parse_params_without_values(self):
        params = server.parse_params("a&b=2&c")
        self.assertEqual(3, len(params))
        self.assertEqual('', params['a'])
        self.assertEqual('2', params['b'])
        self.assertEqual('', params['c'])

    def test_match_params_yes(self):
        expected = [
            {'a': '1'},
            {'b': '2'}
        ]
        actual = server.parse_params("a=1&b=2&c=3")
        self.assertEqual(1, server.match(expected, actual))

    def test_match_params_multivalued_yes(self):
        expected = [
            {'a': ['l1', 'l2', 'l3']},
            {'b': ['4', '5']}
        ]
        actual = server.parse_params("a=l1&a=l2&a=l3&b=4&b=5")
        self.assertEqual(1, server.match(expected, actual))

    def test_match_params_no(self):
        expected = [
            {'a': '1'},
            {'b': '2'}
        ]
        actual = server.parse_params("a=1")
        self.assertEqual(-1, server.match(expected, actual))

    def test_match_params_abstain(self):
        expected = []
        actual = server.parse_params("a=1")
        self.assertEqual(0, server.match(expected, actual))

    def test_match_headers_yes(self):
        expected = [
            {'x-a': '1'},
            {'x-b': '2'}
        ]
        actual = Message()
        actual.add_header('Content-Type', 'text/plain')
        actual.add_header('X-A', '1')
        actual.add_header('X-B', '2')
        self.assertEqual(1, server.match(expected, actual))

    def test_match_headers_no(self):
        expected = [
            {'x-a': '1'},
            {'x-b': '2'}
        ]
        actual = Message()
        actual.add_header('X-A', '1')
        self.assertEqual(-1, server.match(expected, actual))

    def test_match_headers_abstain(self):
        expected = []
        actual = Message()
        actual.add_header('X-A', '1')
        self.assertEqual(0, server.match(expected, actual))


class TestMappingHandler(unittest.TestCase):
    """Test cases for server.MappingHandler class."""

    def setUp(self):
        self.server = server.ConfigHTTPServer(server.load_config('sample/config.yml'), server.MappingHandler)

    def tearDown(self):
        self.server.server_close()

    def test_match_by_path_only(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'GET /data HTTP/1.1\r\n' +
                'Host: localhost\r\n\r\n'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/none.txt', mapping['path'])

    def test_match_by_path_only2(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'GET /data/two HTTP/1.1\r\n' +
                'Host: localhost\r\n\r\n'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/two.txt', mapping['path'])

    def test_match_by_path_and_headers(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'GET /data HTTP/1.1\r\n' +
                'Host: localhost\r\n' +
                'X-Path: one\r\n\r\n'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/one.txt', mapping['path'])

    def test_match_by_path_and_headers_and_post_params(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'POST /data HTTP/1.1\r\n' +
                'Host: localhost\r\n' +
                'Content-Type: application/x-www-form-urlencoded\r\n' +
                'Content-Length: 14\r\n' +
                'X-Path: one\r\n\r\n' +
                'type=a&foo=bar'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/one-a.txt', mapping['path'])

    def test_match_by_path_and_headers_and_post_params_override_query_params(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'POST /data?type=b HTTP/1.1\r\n' +
                'Host: localhost\r\n' +
                'Content-Type: application/x-www-form-urlencoded\r\n' +
                'Content-Length: 14\r\n' +
                'X-Path: one\r\n\r\n' +
                'type=a&foo=bar'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/one-a.txt', mapping['path'])

    def test_match_by_path_and_query_params(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'GET /data?path=three HTTP/1.1\r\n' +
                'Host: localhost\r\n\r\n'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/three.txt', mapping['path'])

    def test_match_by_path_and_query_params_multi_valued(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'GET /data?type=adm&type=mgr&type=mbr HTTP/1.1\r\n' +
                'Host: localhost\r\n\r\n'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/one-admMgrMbr.txt', mapping['path'])

    def test_match_by_path_and_body_pattern(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'PUT /data HTTP/1.1\r\n' +
                'Host: localhost\r\n' +
                'Content-Type: application/json\r\n' +
                'Content-Length: 26\r\n\r\n'
                '{"type":"apple","count":1}'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/ok.json', mapping['path'])

    def test_match_by_jwt_header_claim(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'GET /groups HTTP/1.1\r\n' +
                'Host: localhost\r\n' +
                'Token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mzg2Nzk0ODYsInN1YiI6Imp1c3Rib2IifQ.' +
                'GcJvwensDTJiCle-2EwYRtupq0PwLtpqTdhAUivIpYA'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/groups-justbob.json', mapping['path'])

    def test_match_by_jwt_prefixed_header_with_claim(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'GET /groups HTTP/1.1\r\n' +
                'Host: localhost\r\n' +
                'Authorization: Bearer ' +
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mzg2Nzk0ODYsInN1YiI6Imp1c3Rib2IifQ.' +
                'GcJvwensDTJiCle-2EwYRtupq0PwLtpqTdhAUivIpYA'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/groups-justbob.json', mapping['path'])

    def test_match_by_jwt_cookie_claim(self):
        mock_socket = mock.Mock()
        mock_socket.makefile.return_value = io.BytesIO(
            (
                'GET /groups HTTP/1.1\r\n' +
                'Host: localhost\r\n' +
                'Cookie: token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mzg3Mzg3MjYsInN1YiI6ImNvbm5pZSJ9.' +
                'hPdWUjH2htiBEUtm7Mj6LNOfuBM2HV_ITkf3Ywy0x6E; foo=bar'
            ).encode())
        handler = server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
        mapping = handler.match_request()
        self.assertIsNotNone(mapping)
        self.assertEqual('./sample/data/groups-connie.json', mapping['path'])

    def test_match_round_robin(self):

        def append(x):
            nonlocal response
            response += x.decode('utf-8')

        expected = ['one', 'two', 'three']
        for i in range(5):
            mock_socket = mock.Mock()
            mock_socket.makefile.return_value = io.BytesIO(
                (
                    'GET /multi HTTP/1.1\r\n' +
                    'Host: localhost\r\n\r\n'
                ).encode())
            mock_socket.sendall.side_effect = append
            response = ''
            server.MappingHandler(mock_socket, ('localhost', 8080), self.server)
            lines = response.split('\r\n')
            self.assertEqual(expected[i % 3], lines[-1])


if __name__ == '__main__':
    unittest.main()
