#!/bin/bash

if [ ! -d virtualenv ]; then
  echo "Python virtualenv not found."
  echo "Please set up a Python 3.6 virtualenv in a directory"
  echo "named virtualenv in the project root"
  exit
fi
virtualenv/bin/python -m unittest discover

