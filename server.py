#!/usr/bin/env python

from collections import namedtuple
from os.path import basename
from http import cookies
from http.server import BaseHTTPRequestHandler, HTTPServer

import base64
import io
import json
import re
import ssl
import sys
import yaml


def load_config(path):
    """Load the YAML configuration file at the given path."""
    with io.open(path, 'r') as file:
        cfg_dict = yaml.load(file)
    return namedtuple('Struct', cfg_dict.keys())(*cfg_dict.values())


def parse_params(param_string):
    """Parse a URL-encoded parameter string of the form k1=v1&k2=v2&k3=v3...&kN=vN"""
    params = {}
    if len(param_string) > 0:
        items = [param.split('=') for param in param_string.split('&')]
        for item in items:
            # Tack on an empty value for parameters without values
            if len(item) == 1:
                item.append('')
            k, v = item[0], item[1]
            if k in params:
                if not isinstance(params[k], str):
                    params[k].append(v)
                else:
                    params[k] = [params[k], v]
            else:
                params[k] = v
    return params


def match(expected, actual):
    """Compute a match score for an expected set of request attributes against the actual ones."""
    if not expected:
        return 0
    for mapping in expected:
        key = list(mapping)[0]
        if key not in actual or mapping[key] != actual[key]:
            return -1
    return 1


def decode_jwt(jwt):
    """Decodes the claims in a signed JWT without any HMAC or signature verification."""
    if not jwt:
        return {}
    parts = jwt.split('.')
    if len(parts) != 3:
        return {}
    parts[1] += "=" * ((4 - len(parts[1]) % 4) % 4)
    return json.loads(base64.b64decode(parts[1]))


def freeze(d):
    """Returns a frozen version of the given dict that is hashable."""
    if isinstance(d, dict):
        return frozenset((key, freeze(value)) for key, value in d.items())
    elif isinstance(d, list):
        return tuple(freeze(value) for value in d)
    return d


class ConfigHTTPServer(HTTPServer):
    """Subclass of HTTPServer that holds configuration including a list of mappings used by MappingHandler."""
    def __init__(self, config, handler_class):
        self.config = config
        # Tracks the current index of which file to serve for mappings that specify multiple paths
        self.round_robin_index = {}
        super(ConfigHTTPServer, self).__init__((config.host, config.port), handler_class)


class MappingHandler(BaseHTTPRequestHandler):
    """
    Request handler that compares a list of mappings composed of the following HTTP request characteristics:

    1. Request URI
    2. Request headers
    3. Querystring parameters
    4. POST parameters
    5. JWT claims specified in HTTP header or cookie

    For each request to be handled, the list of mappings is searched for the best match and a match is found,
    the mapping specifies a file and response headers to return. If no match is found a 404 response is returned.
    """
    def __init__(self, request, client_address, server):
        self.body = ''
        self.command = ''
        self.close_connection = True
        self.mappings = server.config.mappings
        self.params = {}
        self.path = ''
        self.raw_requestline = None
        self.requestline = ''
        self.request_version = ''
        super(MappingHandler, self).__init__(request, client_address, server)

    def handle_one_request(self):
        """Override base class method with mapping-specific functionality."""
        self.request.settimeout(1.0)
        try:
            self.setup_request()
            mapping = self.match_request()
            if mapping:
                message = 'Matched ' + str(mapping)
                self.handle_mapping(mapping)
            else:
                message = 'NOT matched'
                self.send_error(404)
            self.wfile.flush()
            print(message)
        except OverflowError:
            # Request line too long
            self.send_error(414)
        except IOError:
            # Empty request line or missing Content-Length header
            self.send_error(400)
        except Warning:
            # Error code already sent by setup_request; nothing to do
            pass
        except Exception as e:
            self.send_error(500, 'Internal Server Error', str(e))
            return

    def send_error(self, code, message=None, explain=None):
        """Override base class method to send error pages in plain text."""
        try:
            short, long = self.responses[code]
        except KeyError:
            short, long = 'Unknown', ''
        if message is None:
            message = short
        if explain is None:
            explain = long
        content = 'ERROR {0}: {1}\n{2}\n'.format(code, message, explain).encode('utf-8')
        self.send_response(code)
        self.send_header("Content-Type", 'text/plain')
        self.send_header('Connection', 'close')
        self.end_headers()
        self.wfile.write(content)

    def setup_request(self):
        self.raw_requestline = self.rfile.readline(65537)
        size = len(self.raw_requestline)
        if size > 65536:
            raise OverflowError('HTTP request line limit exceeded: {0}>65536'.format(size))
        if not self.raw_requestline:
            raise IOError('Empty HTTP request line')
        if not self.parse_request():
            raise Warning('Invalid HTTP request')
        if self.command in ('POST', 'PUT'):
            if 'Content-Length' not in self.headers:
                raise IOError('Content-Length header not found')
            length = int(self.headers['Content-Length'])
            self.body = str(self.rfile.read(length))
        if '?' in self.path:
            (self.path, query) = self.path.split('?')
        else:
            (self.path, query) = (self.path, '')
        self.params = parse_params(query)
        if self.headers['Content-Type'] == 'application/x-www-form-urlencoded':
            self.params.update(parse_params(self.body))

    def handle_mapping(self, mapping):
        """Send a response based on the given matched mapping."""
        content = ''
        if 'path' in mapping:
            if isinstance(mapping['path'], (list,)):
                paths = mapping['path']
                index = 0
                mapping_key = freeze(mapping)
                if mapping_key in self.server.round_robin_index:
                    index = (self.server.round_robin_index[mapping_key] + 1) % len(paths)
                self.server.round_robin_index[mapping_key] = index
                path = paths[index]
            else:
                path = mapping['path']
            with open(path, 'rb') as f:
                content = f.read()
        self.send_response(mapping['response']['code'])
        self.send_header("Content-Length", len(content))
        self.send_header('Connection', 'close')
        for header in mapping['response']['headers']:
            k = list(header)[0]
            self.send_header(k, header[k])
        self.end_headers()
        self.wfile.write(content)

    def match_request(self):
        """Find the most specific mapping that matches the request."""
        max_count = 0
        best_match = None
        for m in self.mappings:
            if m['method'] == self.command and m['uri'] == self.path:
                count = 1
                count += match(m['params'], self.params) if 'params' in m else 0
                count += match(m['headers'], self.headers) if 'headers' in m else 0
                if 'jwt' in m:
                    claims = decode_jwt(self.get_jwt(m['jwt']))
                    count += match(m['jwt']['claims'], claims)
                if 'body-pattern' in m:
                    count += 1 if re.search(m['body-pattern'], self.body) else -1
                if count > max_count:
                    max_count = count
                    best_match = m
        return best_match

    def get_jwt(self, config):
        """Gets the JWT from the request given the configuration that indicates where to look for it."""
        if 'header' in config:
            header = config['header']['name']
            value = self.headers[header]
            if value and 'prefix' in config['header']:
                prefix = config['header']['prefix']
                value = value[len(prefix):]
            return value
        elif 'cookie' in config and 'Cookie' in self.headers:
            c = cookies.SimpleCookie()
            c.load(self.headers['Cookie'])
            morsel = c[config['cookie']]
            return morsel.value if morsel else None
        return None


# --------------------------------------
# Main
# --------------------------------------
if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('USAGE: {} path/to/config.yml'.format(basename(sys.argv[0])))
        sys.exit(0)

    cfg = load_config(sys.argv[1])
    httpd = ConfigHTTPServer(cfg, MappingHandler)
    httpd.socket = ssl.wrap_socket(httpd.socket, certfile=cfg.certfile, server_side=True)
    print('Serving HTTPS requests at {0}:{1}.'.format(cfg.host and cfg.host or '<all interfaces>', cfg.port))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
