Dumb Server: Dumb+Flexible Test Web Server
==========================================

Dumb Server is intended to help mock Web services for integration testing. If your application depends on SOAP/REST
APIs beyond your control, you might consider Dumb Server for the following reasons:

1. There are constraints around provisioning test data into the dependent system.
2. The API requires credentials you can't or don't want to manage in an integration test.
3. Use of the API for testing incurs cost (dollars and/or engineering effort) you can't afford.

Dumb Server uses a truly dumb strategy for serving arbitrary content:

1. A YAML configuration file, config.yml, describes a mapping of requests to responses based on HTTP request artifacts
including HTTP method, URI path, headers, query parameters, POST parameters, and entity body pattern matching.
2. For each request the server iterates over all mappings and chooses the most specific one for the request and
returns it.

An example may help to clarify the behavior; consider the following mapping from the sample config file in the source:

      - uri: "/data"
        method: POST
        path: "./sample/data/one-a.txt"
        headers:
          - x-path: "one"
        params:
          - type: a
        response:
          code: 200
          headers:
            - content-type: "text/plain"

In plain language the following entry reads as follows: If a POST request is made to _/data_ with the header
`X-Path: One` and has either a POST parameter or query parameter of `type=a`, then send a 200 text response with the
content from the file at the relative path _./sample/data/one-a.txt_. Dumb but incredibly flexible.

The full value of a dumb and flexible Web server for integration testing can be realized with Docker. A Docker build
file is provided to run the server as a replacement for a real Web API in a Docker orchestration setting such as
Docker compose. In that case, simply mount a volume with the configuration for that particular API. A docker-compose
example is provided:

    version: "3.4"

    services:
      proxy:
        image: traefik:alpine
        depends_on:
          - cas
          - certs
        command: -c /app/traefik.toml
        ports:
          - 80:80
          - 443:443
          - 8080:8080
        volumes:
          - /var/run/docker.sock:/var/run/docker.sock:ro
          - ./src/local-test/docker/traefik:/app
        networks:
          proxy:
            aliases:
              - cas.internal
              - certs.internal
              - web-server.internal
        labels:
          - traefik.enable=false

      cas:
        image: soulwing/cas-mock-server
        networks:
          - proxy
        labels:
          - traefik.backend=cas
          - traefik.docker.network=certs_proxy
          - traefik.frontend.rule=Host:cas.internal
          - traefik.port=8080

      certs:
        image: code.vt.edu:5005/middleware/certs:latest
        depends_on:
          - certsdb
        networks:
          app:
            ipv4_address: 172.16.10.20
          proxy:
        dns: 172.16.10.10
        ports:
          - "9003:9003"
        volumes:
          - ./src/local-test/docker/certs:/apps/credentials:ro
          - ./target/messages:/apps/data/messages
        environment:
          - ENV=local
          - JAVA_OPTS=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=9003
          - ADDITIONAL_ARGS=--local.domain=internal
        labels:
          - traefik.backend=certs
          - traefik.docker.network=certs_proxy
          - traefik.frontend.rule=Host:certs.internal
          - traefik.protocol=https
          - traefik.port=8443

      certsdb:
        build: ./src/local-test/docker/certsdb
        networks:
          app:
            ipv4_address: 172.16.10.30
        environment:
          - POSTGRES_PASSWORD=intentionallycleartext
        labels:
          - traefik.enable=false

      ed-api:
        image: code.vt.edu:5005/devcom/dumb-server:latest
        volumes:
          - ./src/local-test/docker/ed-api:/data:ro
        networks:
          app:
            ipv4_address: 172.16.10.40

      ejbca-api:
        image: code.vt.edu:5005/devcom/dumb-server:latest
        volumes:
          - ./src/local-test/docker/ejbca-api:/data:ro
        networks:
          app:
            ipv4_address: 172.16.10.50

      incommon-api:
        image: code.vt.edu:5005/devcom/dumb-server:latest
        volumes:
          - ./src/local-test/docker/incommon-api:/data:ro
        networks:
          app:
            ipv4_address: 172.16.10.60


    networks:
      app:
        driver: bridge
        ipam:
          driver: default
          config:
            - subnet: 172.16.10.0/24
      proxy:
